package main

import (
	"testing"
	"net/http/httptest"
	. "github.com/smartystreets/goconvey/convey"
	"net/http"
	"gitlab.com/ello/news-api-go/db"
)

func newTestApp() *App {
	cfg := &Config{
		":8081",
		true,
		db.Config{"localhost:27017"},
	}
	a, _ := newApp(cfg)
	return a
}

func TestGetNewsWrongPath(t *testing.T) {
	a := newTestApp()
	Convey("Given a HTTP request for /invalid/123", t, func() {
		req := httptest.NewRequest("GET", "/invalid/123", nil)
		resp := httptest.NewRecorder()

		Convey("When the request is handled by the Router", func() {
			a.router.ServeHTTP(resp, req)

			Convey("Then the response should be a 404", func() {
				So(resp.Code, ShouldEqual, http.StatusNotFound)
			})
		})
	})
}

func TestGetNews(t *testing.T) {
	a := newTestApp()
	Convey("Given a HTTP request for /news", t, func() {
		req := httptest.NewRequest("GET", "/news", nil)
		resp := httptest.NewRecorder()

		Convey("When the request is handled by the Router", func() {
			a.router .ServeHTTP(resp, req)

			Convey("Then the response should be a 200", func() {
				So(resp.Body.Len(), ShouldBeGreaterThan, 10)
			})
			// todo: model compare
			Convey("Then the response content lent should be a Greater 200", func() {
				So(resp.Body.Len(), ShouldBeGreaterThan, 200)
			})
		})
	})
}
