package controllers

import (
	"net/http"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"gitlab.com/ello/news-api-go/models"
	"log"
	"encoding/json"
	"strconv"
)

const prePage = 20

type newsController struct {
	c *mgo.Collection
}

func NewNewsController(database *mgo.Database) *newsController {

	return &newsController{c:database.C("news")}
}

func (nc *newsController) News(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	log.Print(r.Method, r.URL)

	res := []models.News{}

	page := r.URL.Query().Get("page")
	p, _ := strconv.ParseUint(page, 10, 32)
	s := p * prePage

	nc.c.
		Find(bson.M{}).
		Skip(int(s)).
		Limit(prePage).
		All(&res)

	sendResponse(w, res)
}

func (nc *newsController) NewsById(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	log.Print(r.Method, r.URL)

	id := p.ByName("id")

	if !bson.IsObjectIdHex(id) {
		sendError(w, models.Error{http.StatusNotFound, "Not found"})
		return
	}

	res := models.News{}
	err := nc.c.FindId(bson.ObjectIdHex(id)).
		One(&res)

	if err != nil {
		log.Panic(err)
	}

	// todo: not found

	sendResponse(w, res)
}

func (nc *newsController) NewsAdd(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	log.Print(r.Method, r.URL)

	d := json.NewDecoder(r.Body)
	n := models.News{}

	e1 := d.Decode(&n)

	if e1 != nil {
		log.Println(e1)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	e2 := nc.c.Insert(n)

	if e2 == nil {
		log.Println(http.StatusOK)
		w.WriteHeader(http.StatusOK)
	} else {
		log.Panicln(e2)
		w.WriteHeader(http.StatusInternalServerError)
	}

}

func (nc *newsController) NewsDelete(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	log.Print(r.Method, r.URL)

	id := p.ByName("id")
	err := nc.c.RemoveId(bson.ObjectIdHex(id))
	if err != nil {
		log.Panic(err)
	} else {
		log.Printf("news %s deleted\n", id)
	}
}

func (nc *newsController) NewsEdit(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	id := p.ByName("id")
	d := json.NewDecoder(r.Body)
	n := models.News{}

	e1 := d.Decode(&n)

	if e1 != nil {
		log.Panic(e1)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	e2 := nc.c.UpdateId(bson.ObjectIdHex(id), n)
	if e2 == nil {
		log.Printf("news %s updated\n", id)
		w.WriteHeader(http.StatusOK)
	} else {
		log.Panicln(e2)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (nc *newsController) Reset () {

	m := models.News{
		Title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
		Text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit" +
		 "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." +
		 "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris" +
		 "nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in" +
		 "voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non" +
		 "proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`",
	}

	log.Println("Remove all data in collection ", nc.c.Name)

	// Remove all data in collection
	nc.c.RemoveAll(bson.M{})

	log.Println("Insert example news")
	// Create example news
	for i := 0; i < 1000 ; i++  {
		nc.c.Insert(m)
	}

}

func sendResponse(w http.ResponseWriter, v interface{}) {

	log.Println(http.StatusOK)

	b, _ := json.Marshal(v)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write(b)
}

func sendError(w http.ResponseWriter, e models.Error) {

	log.Println(e.Code)

	b, _ := json.Marshal(e)
	w.WriteHeader(e.Code)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write(b)
}