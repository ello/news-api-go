FROM alpine

WORKDIR /go/src/app
COPY news-api-go /go/src/app/news-api-go

EXPOSE 80

CMD ["news-api-go"]