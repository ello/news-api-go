package db

import (
	"gopkg.in/mgo.v2"
	"log"
)

func InitDb(cfg Config) (*mgo.Database, error)  {

	log.Printf("Connect to MongoDb: %s\n", cfg.ConnectString)

	if session, err := mgo.Dial(cfg.ConnectString); err != nil {
		return nil, err
	} else {
		return session.DB("test"), nil
	}
}
