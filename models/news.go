package models

import "gopkg.in/mgo.v2/bson"

type News struct {
	Id    bson.ObjectId  `json:"id,omitempty" bson:"_id,omitempty"`
	Title string `json:"title"`
	Text  string `json:"text"`
	Tags  []string `json:"tags"`
}
