package main

import (
	"log"
	"flag"
	"gitlab.com/ello/news-api-go/db"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"gitlab.com/ello/news-api-go/controllers"
)

type Config struct {
	ListenSpec string
	Reset      bool
	Db         db.Config
}

type App struct {
	cfg    *Config
	router *httprouter.Router
}

func processFlags() *Config {
	cfg := &Config{}

	flag.StringVar(&cfg.ListenSpec, "listen", "localhost:80", "HTTP listen spec")
	flag.StringVar(&cfg.Db.ConnectString, "db", "localhost:27017", "MongoDB connect string")
	flag.BoolVar(&cfg.Reset, "reset", false, "Reset all data in collection")

	flag.Parse()
	return cfg
}

func main() {

	cfg := processFlags()
	a, err := newApp(cfg)
	if err != nil {
		log.Fatalf("Error initializing app: %v\n", err)
	} else {
		a.start()
	}
}

func newApp(cfg *Config) (*App, error) {

	//a.cfg = cfg

	db, err := db.InitDb(cfg.Db)
	if err != nil {
		log.Printf("Error initializing database: %v\n", err)
		return nil, err
	}

	nc := controllers.NewNewsController(db)

	// Reset data in database
	if cfg.Reset {
		nc.Reset()
	}

	router := httprouter.New()
	router.GET("/news", nc.News)
	router.GET("/news/:id", nc.NewsById)
	router.POST("/news", nc.NewsAdd)
	router.PUT("/news/:id", nc.NewsEdit)
	router.DELETE("/news/:id", nc.NewsDelete)

	a := App{
		cfg,
		router,
	}
	return &a, nil
}

func (a *App) start() {

	if err := http.ListenAndServe(a.cfg.ListenSpec, a.router); err != nil {
		log.Fatalf("Error in ListenAndServe(): %v", err)
	}
}
