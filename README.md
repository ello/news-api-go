### Build image
```bash
GOOS=linux GOARCH=amd64 go build -o news-api-go main.go
docker build .
```